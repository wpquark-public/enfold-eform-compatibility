# eForm Enfold Theme Compatibility

This repository provides some page template files which will play nicely with eForm.

### Installation

1. Download this git repository.
2. Unzip and upload all files under `/wp-content/themes/enfold/` or your child theme.
3. Now create a page and choose eForm noConflict Template and put eForm shortcode.
4. For posts, you can also select eForm noConflict Template and put eForm shortcode.

> This program comes without any guarantee that it will work for future versions.
